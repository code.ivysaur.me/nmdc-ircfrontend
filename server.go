package main

/*
Copyright (C) 2016-2018 The `nmdc-ircfrontend' author(s)
Copyright (C) 2013 Harry Jeffery

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
	"sync"
	"time"

	"code.ivysaur.me/libnmdc"
)

type ClientState int

const (
	CSUnregistered ClientState = iota
	CSRegistered
	CSJoined
)

type Server struct {
	name       string
	motd       string
	hubSecNick string

	clientConn      net.Conn
	clientState     ClientState
	ClientStateLock sync.Mutex

	upstreamLauncher libnmdc.HubConnectionOptions
	upstreamCloser   chan struct{}
	upstreamEvents   chan libnmdc.HubEvent
	upstream         *libnmdc.HubConnection

	verbose  bool
	autojoin bool

	recievedFirstServerMessage bool
	recievedCtcpVersion        bool
	nickChangeAttempt          int
	sentFakeSelfJoin           bool

	quirks Quirks
}

func NewServer(name string, upstream libnmdc.HubAddress, conn net.Conn) *Server {
	self := libnmdc.NewUserInfo("")
	self.ClientTag = APP_NAME
	self.ClientVersion = APP_VERSION

	return &Server{
		name:        name,
		clientConn:  conn,
		clientState: CSUnregistered,
		motd:        "Connected to " + name + ". You /must/ join " + BLESSED_CHANNEL + " to continue.",
		upstreamLauncher: libnmdc.HubConnectionOptions{
			Address:           upstream,
			Self:              self,
			SkipAutoReconnect: true,
		},
		upstreamEvents: make(chan libnmdc.HubEvent, 0), // unbuffered
		upstreamCloser: make(chan struct{}, 1),
		quirks:         Quirks{},
	}
}

func (s *Server) verboseln(line string) {
	if s.verbose {
		log.Println(line)
	}
}

func (s *Server) verbosef(fmt string, args ...interface{}) {
	if s.verbose {
		log.Printf(fmt, args...)
	}
}

func (s *Server) RunWorker() {

	// Send the connection handshake.
	// Can't connect to the upstream server yet, until we've recieved a nick.
	s.sendMOTD(s.motd)

	for {
		if s.clientConn == nil {
			break // abandon thread
		}

		buf := make([]byte, CLIENT_READ_BUFFSIZE)

		s.clientConn.SetReadDeadline(time.Now().Add(CLIENT_KEEPALIVE_EVERY * time.Second))
		ln, err := s.clientConn.Read(buf)

		if err != nil {
			if err == io.EOF {
				break // abandon thread
			}

			// If this was a /timeout/, send a KA and continue.
			if netErr, ok := err.(net.Error); ok && netErr.Timeout() {
				s.writeClient("PING :" + s.name)
				continue
			}

			// But otherwise, it was a real error (e.g. unexpected disconnect)
			s.verboseln(err.Error())
			break // abandon
		}

		rawLines := buf[:ln]
		rawLines = bytes.Replace(rawLines, []byte("\r\n"), []byte("\n"), -1)
		rawLines = bytes.Replace(rawLines, []byte("\r"), []byte("\n"), -1)
		lines := bytes.Split(rawLines, []byte("\n"))
		for _, line := range lines {
			if len(line) > 0 {

				// Client sent a command
				fields := strings.Fields(string(line))
				if len(fields) == 0 {
					return
				}

				if strings.HasPrefix(fields[0], ":") {
					fields[0] = fields[0][1:]
				}

				s.handleCommand(strings.ToUpper(fields[0]), fields[1:])
			}
		}
	}

	s.verboseln("Broken loop.")

	// Cleanup upstream
	if s.upstream != nil {
		s.upstreamCloser <- struct{}{} // always safe to do this /once/
	}

	// Clean up ourselves
	s.DisconnectClient() // if not already done
}

func (s *Server) postGeneralMessageInRoom(msg string) {

	var sendAs = ""

	// Some clients can't handle blank nicks very well
	if s.quirks.RequireNickForGeneralMessages {
		sendAs = s.hubSecNick + "!" + s.hubSecNick + "@" + s.hubSecNick
	}

	// Detect pseudo-system message for potential CTCP ACTION conversion
	words := strings.Split(msg, " ")
	firstWord := words[0]
	remainder := strings.Join(words[1:], " ")
	if firstWord == "*" {
		firstWord = words[1]
		remainder = strings.Join(words[2:], " ")
	}

	if s.upstream.UserExists(firstWord) {
		// it's a /me in disguise - convert back to a CTCP ACTION
		// If it's **our own** action, skip it
		if firstWord != s.clientNick() {
			s.reply(
				rplMsg, firstWord+"!"+firstWord+"@"+firstWord, BLESSED_CHANNEL,
				"\x01ACTION "+remainder+"\x01",
			)
		}

	} else {
		// genuine system message
		s.reply(rplMsg, sendAs, BLESSED_CHANNEL, msg)
	}

}

func (s *Server) upstreamWorker() {

	// Read loop
	for {
		select {
		case <-s.upstreamCloser:
			// Abandon the upstream connection
			s.verboseln("Abandoning upstream connection...")
			s.upstream.Disconnect()
			return

		case hubEvent := <-s.upstreamEvents:
			switch hubEvent.EventType {
			case libnmdc.EVENT_USER_JOINED:
				if hubEvent.Nick == s.clientNick() && s.sentFakeSelfJoin {
					s.sentFakeSelfJoin = false
				} else {
					// If we want to JOIN with the full power of the supplied nick!user@host, then we'll need to actually remember the active client's USER parameters
					s.reply(rplJoin, hubEvent.Nick, BLESSED_CHANNEL)
				}

			case libnmdc.EVENT_USER_PART:
				s.reply(rplPart, hubEvent.Nick, BLESSED_CHANNEL, "Disconnected")

			case libnmdc.EVENT_USER_UPDATED_INFO:
				// description change - no relevance for IRC users

			case libnmdc.EVENT_CONNECTION_STATE_CHANGED:
				s.postGeneralMessageInRoom("* Upstream: " + hubEvent.StateChange.String())

				if hubEvent.StateChange == libnmdc.CONNECTIONSTATE_CONNECTED {
					s.sendNames() // delay doing this until now
				}
				if hubEvent.StateChange == libnmdc.CONNECTIONSTATE_DISCONNECTED {
					if s.nickChangeAttempt > 0 {
						// If this was a nick change, reconnect /immediately/
						s.upstream = nil
						s.clientState = CSRegistered
						s.maybeStartUpstream() // launches new goroutine

					} else {
						// Abandon thread. Don't try to autoreconnect at our level, the remote client can be responsible for that
						s.DisconnectClient()

					}
					return
				}

			case libnmdc.EVENT_HUBNAME_CHANGED:
				s.sendChannelTopic(hubEvent.Nick)

			case libnmdc.EVENT_PRIVATE:
				s.reply(rplMsg, hubEvent.Nick+"!"+hubEvent.Nick+"@"+hubEvent.Nick, s.clientNick(), reformatIncomingMessageBody(hubEvent.Message))

			case libnmdc.EVENT_PUBLIC:
				if hubEvent.Nick == s.clientNick() {
					// irc doesn't echo our own pubchat
				} else {
					// nick!username@userhost, but for us all three of those are always identical
					s.reply(rplMsg, hubEvent.Nick+"!"+hubEvent.Nick+"@"+hubEvent.Nick, BLESSED_CHANNEL, reformatIncomingMessageBody(hubEvent.Message))
				}

				if !s.recievedFirstServerMessage {
					s.hubSecNick = hubEvent.Nick // Replace with the hub's real Hub-Security nick, although we shouldn't need it again
				}

			case libnmdc.EVENT_SYSTEM_MESSAGE_FROM_CONN, libnmdc.EVENT_SYSTEM_MESSAGE_FROM_HUB:
				s.postGeneralMessageInRoom(hubEvent.Message)

			}
		}
	}
}

func (s *Server) handleCommand(command string, args []string) {

	s.verbosef(" >>> '%s' %v", command, args)

	switch command {
	case "PING":
		s.reply(rplPong, strings.Join(args, " "))

	case "PONG":
		// do nothing

	case "INFO":
		s.reply(rplInfo, APP_NAME+" v"+APP_VERSION)

	case "VERSION":
		s.reply(rplVersion, APP_VERSION)

	case "MOTD":
		s.sendMOTD(s.motd)

	case "CAP":
		return
		/*
			if len(args) < 1 {
				s.reply(errMoreArgs)
				return
			}

			if args[0] == "LS" {
				s.writeClient("CAP * LS :nmdc-ircfrontend") // no special IRCv3 capabilities available
			} else {
				s.writeClient(fmt.Sprintf(":%s 410 * %s :Invalid CAP command", s.name, args[0]))
			}
		*/

	case "PASS":
		// RFC2812 registration. Stash the password for later
		if len(args) < 1 {
			s.reply(errMoreArgs)
			return
		}
		s.upstreamLauncher.NickPassword = args[0]

	case "NICK":
		if len(args) < 1 {
			s.reply(errMoreArgs)
			return
		}

		// mIRC puts a colon in first place when changing nick, hexchat doesn't
		suppliedNick := args[0]
		if len(suppliedNick) >= 2 && suppliedNick[0] == ':' {
			suppliedNick = suppliedNick[1:]
		}

		if s.clientNick() == "" {
			// allow set, as part of the login phase
			s.upstreamLauncher.Self.Nick = suppliedNick

		} else if suppliedNick == s.clientNick() {
			// Ignore
			// Required for compatibility with Lite IRC, which sends USER/NICK in the wrong order

		} else {

			s.ClientStateLock.Lock()
			defer s.ClientStateLock.Unlock()
			if s.upstream == nil {
				// Not yet connected, should be safe to change nick
				s.upstreamLauncher.Self.Nick = suppliedNick
			} else {
				// Need to disconnect/reconnect the upstream
				s.writeClient(fmt.Sprintf(":%s!%s@%s NICK %s", s.clientNick(), s.clientNick(), s.clientNick(), suppliedNick)) // notify client about what they've done
				s.upstreamLauncher.Self.Nick = suppliedNick

				s.nickChangeAttempt++
				s.upstream.Disconnect()
			}

			//s.reply(rplKill, "Can't change nicks on this server.")
			//s.DisconnectClient()
		}

	case "USER":
		// This command sets altname, realname, ... none of which we use
		// It's the final step in a PASS/NICK/USER login handshake.

		if s.clientState != CSUnregistered {
			s.reply(rplKill, "You're already registered.")
			s.DisconnectClient()
			return
		}

		if s.clientNick() == "" {
			// Whatever, treat it as a NICK call (non-strict client handshake) and take the username field to be the intended nick
			// This will allow Lite IRC's bad handshake to log in (as long as the username and the nickname are the same)
			s.upstreamLauncher.Self.Nick = args[0]
		}

		// Use the client's {real name} field as an NMDC {description}
		if len(args) >= 4 && len(args[3]) > 0 && args[3][0] == ':' {
			realName := strings.Join(args[3:], " ")[1:]
			s.upstreamLauncher.Self.Description = realName
		}

		s.reply(rplWelcome)
		s.clientState = CSRegistered

		// Send CTCP VERSION request immediately
		s.reply(rplMsg, s.hubSecNick+"!"+s.hubSecNick+"@"+s.hubSecNick, BLESSED_CHANNEL, "\x01VERSION\x01")

		if s.autojoin {
			s.handleCommand("JOIN", []string{BLESSED_CHANNEL})
		}

	default:
		s.handleRegisteredCommand(command, args)
	}
}

func (s *Server) handleRegisteredCommand(command string, args []string) {

	if s.clientState == CSUnregistered {
		s.reply(errNotReg)
		return
	}

	switch command {
	case "LIST":
		if s.upstream == nil {
			s.reply(rplList, fmt.Sprintf("%s %d :%s", BLESSED_CHANNEL, 1, "-"))
		} else {
			s.reply(rplList, fmt.Sprintf("%s %d :%s", BLESSED_CHANNEL, s.upstream.UserCount(), s.upstream.HubName))
		}
		s.reply(rplListEnd)

	case "PRIVMSG", "NOTICE":
		if len(args) < 2 {
			s.reply(errMoreArgs)
			return
		}

		// Strip leading colon (work around a bug in HoloIRC 4.1.0 and older)
		message := strings.Join(args[1:], " ")[1:]

		if strings.HasPrefix(message, "\x01VERSION ") {
			// Not a real message - a reply to our internal request. Change the user's tag to match the actual client software
			// This /can/ actually be done regardless of whether we're truely connected yet, since the request is triggered
			//   by an incoming PM which means even though we might not be in CONNECTIONSTATE_CONNECTED, it's probably far-enough
			//   that an extra upstream $MyINFO won't hurt
			versionString := message[9:]
			versionString = versionString[:len(versionString)-1]
			s.SetClientSoftwareVersion(versionString)
			s.quirks = GetQuirksForClient(versionString)
			return
		}

		if s.upstream == nil || s.upstream.State != libnmdc.CONNECTIONSTATE_CONNECTED {
			s.reply(errCannotSend, args[0])
			return
		}

		if strings.HasPrefix(message, "\x01ACTION ") {
			message = "/me " + message[8:]
			message = message[:len(message)-1] // trailing \x01
		}

		// IRC is case-insensitive case-preserving. We can respect that for the
		//  channel name, but not really for user nicks

		if strings.ToLower(args[0]) == BLESSED_CHANNEL {
			s.upstream.SayPublic(reformatOutgoingMessageBody(message))

		} else if s.upstream.UserExists(args[0]) {
			s.upstream.SayPrivate(args[0], reformatOutgoingMessageBody(message))

		} else {
			s.reply(errNoSuchNick, args[0])

		}

	case "JOIN":
		if len(args) < 1 {
			s.reply(errMoreArgs)
			return
		}

		switch args[0] {
		case BLESSED_CHANNEL:
			// Give it a few seconds - it's better to hear the CTCP VERSION
			//   response first. Once we get that, it'll connect instantly
			go func() {
				<-time.After(WAIT_FOR_VERSION * time.Second)

				s.ClientStateLock.Lock()
				defer s.ClientStateLock.Unlock()

				s.maybeStartUpstream()
			}()

		case "0":
			// Quitting all channels? Drop client
			s.reply(rplKill, "Bye.")
			s.DisconnectClient()

		default:
			s.reply(rplKill, "There is only '"+BLESSED_CHANNEL+"'.")
			s.DisconnectClient()
		}

	default:
		s.handleJoinedCommand(command, args)
	}
}

func (s *Server) maybeStartUpstream() {
	if s.clientState != CSJoined {
		// Join for the first time
		s.clientState = CSJoined

		// Acknowledge
		s.reply(rplJoin, s.clientNick(), BLESSED_CHANNEL)

		// Spawn upstream connection
		s.upstream = libnmdc.ConnectAsync(&s.upstreamLauncher, s.upstreamEvents)
		go s.upstreamWorker()

	} else {
		// They're already here, ignore
		// Can happen if autojoin is enabled but the client already requested a login
	}
}

func (s *Server) SetClientSoftwareVersion(ver string) {

	ct := parseVersion(ver)

	s.verbosef("Replacing client tag with '%s' version '%s'", ct.AppName, ct.Version)

	s.upstreamLauncher.Self.ClientTag = ct.AppName
	s.upstreamLauncher.Self.ClientVersion = ct.Version
	s.recievedCtcpVersion = true

	s.ClientStateLock.Lock()
	defer s.ClientStateLock.Unlock()

	if s.upstream != nil {
		s.upstream.Hco.Self.ClientTag = ct.AppName
		s.upstream.Hco.Self.ClientVersion = ct.Version
		s.upstream.SayInfo()

	} else {
		// Connected for the first time (order was CTCP VERSION --> JOIN)

		s.maybeStartUpstream()

	}

}

func (s *Server) handleJoinedCommand(command string, args []string) {

	if s.clientState != CSJoined {
		s.reply(errNotReg)
		return
	}

	switch command {
	case "PART":
		if len(args) < 1 {
			s.reply(errMoreArgs)
			return
		}

		if args[0] == BLESSED_CHANNEL {
			// You can check out any time you like, but you can never leave
			s.reply(rplJoin, s.clientNick(), BLESSED_CHANNEL)
			s.sendNames()
		}

	case "QUIT":
		s.DisconnectClient()

	case "TOPIC":
		if len(args) < 1 {
			s.reply(errMoreArgs)
			return
		}

		if strings.ToLower(args[0]) != BLESSED_CHANNEL {
			s.reply(errNoSuchNick, args[0])
			return
		}

		if len(args) == 1 {
			s.sendChannelTopic(s.upstream.HubName) // Valid topic get
		} else {
			s.reply(errNoPriv) // Disallow topic set
		}

	case "PROTOCTL":
		// we advertised support for NAMESX, if this happens the client accepted it
		s.sendNames()

	case "OPER":
		if len(args) < 2 {
			s.reply(errMoreArgs)
			return
		}

		// Can't use this command.
		s.reply(errPassword)

	case "KILL", "KICK":
		s.reply(errNoPriv)
		return

	case "WHO":
		if len(args) < 1 {
			s.reply(errMoreArgs)
			return
		}

		// Ignore this command

	case "MODE":
		if len(args) < 1 {
			s.reply(errMoreArgs)
			return
		}

		if strings.ToLower(args[0]) != BLESSED_CHANNEL {
			s.reply(errNoSuchNick, args[0])
			return
		}

		if len(args) == 1 {
			// No more args, they just want the mode
			s.sendChannelMode()

		} else {
			// Setting modes is disallowed
			s.reply(errNoPriv)
		}

		return

	case "WHOIS":
		if len(args) < 1 {
			s.reply(errMoreArgs)
			return
		}

		// WHOIS [target] nick[,nick2[,nick...]]
		nicklist := args[0] // Assume WHOIS ${nick} only,
		if len(args) >= 2 {
			nicklist = args[1] // It was WHOIS ${target} ${nick} instead
		}

		for _, targetnick := range strings.Split(nicklist, ",") {
			// tell the client something about it
			// The protocol does ostensibly support wildcard WHOIS, but we don't (yet)
			s.upstream.Users(func(u *map[string]libnmdc.UserInfo) error {
				for nick, nickinfo := range *u {
					if nick == targetnick {
						s.reply(rplWhoisUser, nick, nickinfo.Description+" <"+nickinfo.ClientTag+" V:"+nickinfo.ClientVersion+">")
						if nickinfo.IsOperator {
							s.reply(rplWhoisOperator, nick)
						}
					}
				}
				return nil
			})
			s.reply(rplEndOfWhois)
		}

	default:
		s.reply(errUnknownCommand, command)
	}
}

func (s *Server) DisconnectClient() {
	if s.clientConn != nil {
		s.clientConn.Close()
	}
	s.clientConn = nil
	s.clientState = CSUnregistered

	// Readloop will stop, which kills the upstream connection too
}

func (s *Server) sendNames() {

	nameList := make([]string, 0)

	if s.upstream != nil {
		s.upstream.Users(func(u *map[string]libnmdc.UserInfo) error {
			for nick, nickinfo := range *u {
				if nickinfo.IsOperator {
					nameList = append(nameList, "@"+nick)
				} else {
					nameList = append(nameList, nick)
				}
			}
			return nil
		})
	}

	if len(nameList) == 0 {
		// We don't have a nick list yet. Many clients can't handle a blank list
		// We could delay until we do have a nick list
		// Or, we could send our nick only, and filter it out of the next join

		nameList = append(nameList, s.clientNick())
		s.sentFakeSelfJoin = true
	}

	s.reply(rplNames, BLESSED_CHANNEL, strings.Join(nameList, " "))
	s.reply(rplEndOfNames, BLESSED_CHANNEL)
}

func (s *Server) sendChannelMode() {
	s.reply(rplChannelModeIs, BLESSED_CHANNEL, BLESSED_CHANNEL_MODE, "")
}

func (s *Server) sendWho(arg string) {
	if arg == BLESSED_CHANNEL {
		// always include ourselves
		s.reply(rplWho, s.clientNick(), arg)

		s.upstream.Users(func(u *map[string]libnmdc.UserInfo) error {
			for nick, _ := range *u {
				if nick != s.clientNick() { // but don't repeat ourselves
					s.reply(rplWho, nick, arg)
				}
			}
			return nil
		})
	} else {
		// argument is a filter
		s.upstream.Users(func(u *map[string]libnmdc.UserInfo) error {
			for nick, _ := range *u {
				if strings.Contains(nick, arg) {
					s.reply(rplWho, nick, arg)
				}
			}
			return nil
		})
	}
	s.reply(rplEndOfWho, arg)
}

func (s *Server) sendChannelTopic(topic string) {
	if len(topic) > 0 {
		s.reply(rplTopic, BLESSED_CHANNEL, s.upstream.HubName)
	} else {
		s.reply(rplNoTopic, BLESSED_CHANNEL)
	}
}

func (s *Server) sendMOTD(motd string) {
	s.reply(rplMOTDStart)

	for len(motd) > 80 {
		s.reply(rplMOTD, motd[:80])
		motd = motd[80:]
	}
	if len(motd) > 0 {
		s.reply(rplMOTD, motd)
	}

	s.reply(rplEndOfMOTD)
}

func (s *Server) clientNick() string {
	return s.upstreamLauncher.Self.Nick
}

// Send a reply to a user with the code specified
func (s *Server) reply(code replyCode, args ...string) {

	switch code {
	case rplWelcome:
		s.writeClient(fmt.Sprintf(":%s 001 %s :Welcome to %s", s.name, s.clientNick(), s.name))
		s.writeClient(fmt.Sprintf(":%s 005 %s NAMESX CHANTYPES=# :are supported by this server", s.name, s.clientNick()))

	case rplJoin:
		s.writeClient(fmt.Sprintf(":%s!%s@%s JOIN %s", args[0], args[0], args[0], args[1]))
	case rplPart:
		s.writeClient(fmt.Sprintf(":%s!%s@%s PART %s %s", args[0], args[0], args[0], args[1], args[2]))
	case rplTopic:
		s.writeClient(fmt.Sprintf(":%s 332 %s %s :%s", s.name, s.clientNick(), args[0], args[1]))
	case rplNoTopic:
		s.writeClient(fmt.Sprintf(":%s 331 %s %s :No topic is set", s.name, s.clientNick(), args[0]))

	case rplNames:
		s.writeClient(fmt.Sprintf(":%s 353 %s = %s :%s", s.name, s.clientNick(), args[0], args[1]))
	case rplEndOfNames:
		s.writeClient(fmt.Sprintf(":%s 366 %s %s :End of /NAMES list.", s.name, s.clientNick(), args[0]))

	case rplWho:
		s.writeClient(fmt.Sprintf(":%s 352 %s %s %s %s %s %s H :0 %s", s.name, s.clientNick(), args[1], args[0], args[0], s.name, args[0], args[0]))
	case rplEndOfWho:
		s.writeClient(fmt.Sprintf(":%s 315 %s %s :End of WHO list", s.name, s.clientNick(), args[0]))

	case rplNickChange:
		s.writeClient(fmt.Sprintf(":%s NICK %s", args[0], args[1]))

	case rplKill:
		s.writeClient(fmt.Sprintf(":%s KILL %s %s", s.name, s.clientNick(), args[0]))
		// s.writeClient(fmt.Sprintf(":%s KILL %s A %s", args[0], s.clientNick(), args[1]))

	case rplMsg:
		for _, itm := range strings.Split(args[2], "\n") {
			s.writeClient(fmt.Sprintf(":%s PRIVMSG %s :%s", args[0], args[1], itm))
		}

	case rplList:
		s.writeClient(fmt.Sprintf(":%s 322 %s %s", s.name, s.clientNick(), args[0]))
	case rplListEnd:
		s.writeClient(fmt.Sprintf(":%s 323 %s", s.name, s.clientNick()))

	case rplOper:
		s.writeClient(fmt.Sprintf(":%s 381 %s :You are now an operator", s.name, s.clientNick()))
	case rplChannelModeIs:
		s.writeClient(fmt.Sprintf(":%s 324 %s %s %s %s", s.name, s.clientNick(), args[0], args[1], args[2]))
	case rplKick:
		s.writeClient(fmt.Sprintf(":%s KICK %s %s %s", args[0], args[1], args[2], args[3]))
	case rplInfo:
		s.writeClient(fmt.Sprintf(":%s 371 %s :%s", s.name, s.clientNick(), args[0]))
	case rplVersion:
		s.writeClient(fmt.Sprintf(":%s 351 %s %s", s.name, s.clientNick(), args[0]))

	case rplMOTDStart:
		s.writeClient(fmt.Sprintf(":%s 375 %s :- Message of the day - ", s.name, s.clientNick()))
	case rplMOTD:
		s.writeClient(fmt.Sprintf(":%s 372 %s :- %s", s.name, s.clientNick(), args[0]))
	case rplEndOfMOTD:
		s.writeClient(fmt.Sprintf(":%s 376 %s :- End of MOTD", s.name, s.clientNick()))

	case rplPong:
		s.writeClient(fmt.Sprintf(":%s PONG %s %s", s.name, s.clientNick(), args[0]))

	case rplWhoisUser:
		s.writeClient(fmt.Sprintf(":%s 311 %s %s %s %s * :%s", s.name, args[0], args[0], args[0], s.name, args[1])) // caller should supply nick,description
	case rplWhoisOperator:
		s.writeClient(fmt.Sprintf(":%s 313 %s :is an IRC operator", s.name, args[0]))
	case rplEndOfWhois:
		s.writeClient(fmt.Sprintf(":%s 318 :End of WHOIS list", s.name))

	case errMoreArgs:
		s.writeClient(fmt.Sprintf(":%s 461 %s :Not enough params", s.name, s.clientNick()))
	case errNoNick:
		s.writeClient(fmt.Sprintf(":%s 431 %s :No nickname given", s.name, s.clientNick()))
	case errInvalidNick:
		s.writeClient(fmt.Sprintf(":%s 432 %s %s :Erronenous nickname", s.name, s.clientNick(), args[0]))
	case errNickInUse:
		s.writeClient(fmt.Sprintf(":%s 433 %s %s :Nick already in use", s.name, s.clientNick(), args[0]))
	case errAlreadyReg:
		s.writeClient(fmt.Sprintf(":%s 462 :You need a valid nick first", s.name))
	case errNoSuchNick:
		s.writeClient(fmt.Sprintf(":%s 401 %s %s :No such nick/channel", s.name, s.clientNick(), args[0]))
	case errUnknownCommand:
		s.writeClient(fmt.Sprintf(":%s 421 %s %s :Unknown command", s.name, s.clientNick(), args[0]))
	case errNotReg:
		s.writeClient(fmt.Sprintf(":%s 451 :- You have not registered", s.name))
	case errPassword:
		s.writeClient(fmt.Sprintf(":%s 464 %s :Error, password incorrect", s.name, s.clientNick()))
	case errNoPriv:
		s.writeClient(fmt.Sprintf(":%s 481 %s :Permission denied", s.name, s.clientNick()))
	case errCannotSend:
		s.writeClient(fmt.Sprintf(":%s 404 %s %s :Cannot send to channel", s.name, s.clientNick(), args[0]))
	}
}

func (s *Server) writeClient(output string) {
	if s.clientConn == nil {
		return
	}

	s.verbosef(" <<< %s", output)

	s.clientConn.SetWriteDeadline(time.Now().Add(time.Second * 30))
	if _, err := fmt.Fprintf(s.clientConn, "%s\r\n", output); err != nil {
		s.DisconnectClient()
		return
	}
}
