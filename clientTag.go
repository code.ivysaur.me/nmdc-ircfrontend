package main

import (
	"regexp"
	"strings"
)

type clientTag struct {
	AppName string
	Version string
}

var (
	rx_bestNumberPart = regexp.MustCompile(`[0-9\.]+`)
)

func parseVersion(ver string) clientTag {
	// Try our best to turn the supplied text into a structured version
	ret := clientTag{
		AppName: APP_NAME,
		Version: APP_VERSION,
	}

	// Special case: Some clients use a structured version AppName:Version:Metadata
	// If we check for that, we can support clients with spaces in the name
	if cParts := strings.Split(ver, ":"); len(cParts) == 3 {
		ret.AppName = cParts[0]
		ret.Version = cParts[1]

	} else if ver == "Atomic - An IRC client for Android https://indrora.github.io/Atomic" {
		ret.AppName = "Atomic"
		ret.Version = "2.1" // Abandonware. Last available version

	} else if ver == "Yaaic - Yet Another Android IRC Client - http://www.yaaic.org" {
		ret.AppName = "Yaaic"
		ret.Version = "1.1" // Abandonware. Last available version

	} else {

		// Special cases all failed, time for heuristics
		// Turn colons to spaces; keep the first word, and the the first word containing digits
		ver = strings.Trim(strings.Replace(ver, ":", " ", -1), " ")

		words := strings.Split(ver, " ")

		for _, word := range words[1:] {
			if strings.ContainsAny(word, "0123456789") {
				ret.AppName = words[0]
				ret.Version = strings.Replace(word, "(", "", -1) // AndroIRC
				break
			}
		}
	}

	// We only support digits, periods, and hyphens in the number part
	// This removes the leading v from mIRC and the trailing deb** from irssi
	if submatch := rx_bestNumberPart.FindStringSubmatch(ret.Version); len(submatch) == 1 {
		ret.Version = submatch[0]
	}

	// Special case: "Relay" means "HoloIRC"
	if ret.AppName == "Relay" && ret.Version == "1.0" && strings.Contains(ver, "Android") {
		ret.AppName = "HoloIRC"
		ret.Version = "4"
	}

	return ret
}
