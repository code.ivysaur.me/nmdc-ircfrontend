package main

import (
	"regexp"
	"strings"
)

var (
	rx_colorControlCode      *regexp.Regexp = regexp.MustCompilePOSIX("\x03[0-9]+(,[0-9]+)?")
	rx_formattingControlCode *regexp.Regexp = regexp.MustCompile("(\x02|\x1D|\x1F|\x16|\x0F)")
)

func reformatOutgoingMessageBody(body string) string {

	// Strip color codes
	noColors := rx_colorControlCode.ReplaceAllString(body, "")

	// Strip formatting codes
	return rx_formattingControlCode.ReplaceAllString(noColors, "")

}

func reformatIncomingMessageBody(body string) string {

	// "Greentext" filter
	// TODO markdown filters
	if len(body) > 0 && body[0] == '>' {
		return "\x033" + strings.Replace(body, "implying", "\x02implying\x02", -1)

	} else {
		return body
	}
}
