package main

/*
Copyright (C) 2016-2018 The `nmdc-ircfrontend' author(s)
Copyright (C) 2013 Harry Jeffery

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import (
	"flag"
	"log"
	"net"

	"code.ivysaur.me/libnmdc"
)

func main() {

	ircAddress := flag.String("bind", ":6667", "The address:port to bind to and listen for clients on")
	dcAddress := flag.String("upstream", "127.0.0.1:411", "Upstream NMDC/ADC server")
	serverName := flag.String("servername", APP_NAME, "Server name displayed to clients")
	hubsec := flag.String("hubsecurity", "Hub-Security", "Nick used for administrative events")
	verbose := flag.Bool("verbose", false, "Display debugging information")
	autojoin := flag.Bool("autojoin", true, "Automatically join clients to the channel")
	version := flag.Bool("version", false, "Display version and exit")

	flag.Parse()

	if *version {
		log.Printf("%s version %s\n", APP_NAME, APP_VERSION)
		return
	}

	log.Printf("Listening on '%s'...", *ircAddress)
	if *autojoin {
		log.Printf("Clients will be automatically joined to the channel.")
	}

	listener, err := net.Listen("tcp", *ircAddress)
	if err != nil {
		log.Printf(err.Error())
		return
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("Error accepting connection (%s)", err.Error())
			continue
		}

		// Spin up a worker for the new user.
		server := NewServer(*serverName, libnmdc.HubAddress(*dcAddress), conn)
		server.verbose = *verbose
		server.autojoin = *autojoin
		server.hubSecNick = *hubsec
		go server.RunWorker()
	}
}
