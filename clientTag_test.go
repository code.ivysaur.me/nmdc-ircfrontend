package main

import (
	"testing"
)

func TestParseVersion(t *testing.T) {

	tests := [][3]string{

		[3]string{
			"HexChat 2.12.1 [x64] / Microsoft Windows 10 Pro (x64) [Intel(R) Core(TM) i5-2500 CPU @ 3.30GHz (3.60GHz)]",
			"HexChat", "2.12.1",
		},

		[3]string{
			"AndroIRC - Android IRC Client (5.2 - Build 6830152) - http://www.androirc.com",
			"AndroIRC", "5.2",
		},

		[3]string{
			"AndChat 1.4.3.2 http://www.andchat.ne",
			"AndChat", "1.4.3.2",
		},

		[3]string{
			"liteIRC for Android 1.1.8",
			"liteIRC", "1.1.8",
		},

		[3]string{
			":Relay:1.0:Android", // HoloIRC before 4.1.0
			"HoloIRC", "4",
		},

		[3]string{
			"Relay:1.0:Android", // HoloIRC after 4.1.0
			"HoloIRC", "4",
		},

		[3]string{
			"mIRC v7.45",
			"mIRC", "7.45",
		},

		[3]string{
			"Revolution IRC:0.3.2:Android",
			"Revolution IRC", "0.3.2",
		},

		[3]string{
			"irssi v1.0.2-1+deb9u3",
			"irssi", "1.0.2",
		},

		[3]string{
			"Atomic - An IRC client for Android https://indrora.github.io/Atomic",
			"Atomic", "2.1",
		},

		[3]string{
			"Yaaic - Yet Another Android IRC Client - http://www.yaaic.org",
			"Yaaic", "1.1",
		},
	}

	for _, test := range tests {
		result := parseVersion(test[0])
		if result.AppName != test[1] || result.Version != test[2] {
			t.Fatalf("Got <%s,%s> expecting <%s,%s>\n", result.AppName, result.Version, test[1], test[2])
		}
	}
}
