package main

import (
	"strings"
)

type Quirks struct {
	RequireNickForGeneralMessages bool
}

func GetQuirksForClient(ver string) Quirks {
	if strings.Contains(ver, "mIRC") || strings.Contains(ver, "Atomic") || strings.Contains(ver, "Yaaic") {
		return Quirks{
			RequireNickForGeneralMessages: true,
		}

	} else {
		return Quirks{}
	}
}
