# References

https://www.alien.net.au/irc/irc2numerics.html

http://faerion.sourceforge.net/doc/irc/whox.var

http://www.anta.net/misc/telnet-troubleshooting/irc.shtml

https://tools.ietf.org/html/rfc2812

http://www.irchelp.org/irchelp/rfc/ctcpspec.html

https://en.wikipedia.org/wiki/List_of_Internet_Relay_Chat_commands

https://en.wikipedia.org/wiki/Client-to-client_protocol

https://wiki.mibbit.com/index.php/Ctcp_(version)

https://github.com/eXeC64/Rosella AGPLv3 license

https://github.com/edmund-huber/ergonomadic MIT license

http://en.wikichip.org/wiki/irc/colors
