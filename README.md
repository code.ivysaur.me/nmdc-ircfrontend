# nmdc-ircfrontend

An IRC protocol frontend for an NMDC server.

This program 'wraps' an NMDC server to make it available to IRC clients. It implements the IRC server protocol, but is not itself an IRC server, deferring to the upstream NMDC server in all salient cases. From a layering perspective it occupies the same position as the web interface.

The intent of this project is to expand client library and application support on platforms where the NMDC community has not been able to spend effort (e.g. Apple iOS client, ruby/rust/erlang libraries, greater choice in web interfaces).

TLS (SSL) support is not integrated. To host the IRC frontend over TLS, please use a reverse proxy in much the same manner as for NMDCS.

## License

This program uses some code from the AGPLv3 project https://github.com/eXeC64/Rosella , from which it inherits the Affero GPLv3 license. Anyone hosting a modified version of this software is required to release their changes under the terms of the AGPLv3.

## Features

- Seamless roundtrip transition between NMDC `/me` and IRC `CTCP ACTION`
- IRC client's "Real Name" exposed as NMDC Description
- IRC client's `CTCP VERSION` exposed as NMDC client tag, with heuristic truncation
- NMDC server's title exposed as IRC channel topic
- Login with passworded nick (classic `PASS`, not `SASL`)
- Full nick list integration including op status
- Support IRC client changing nick via NMDC upstream reconnection
- Automatic join to single-enforced chatroom
- Multithreaded
- Single binary deployment

## Usage

```
Usage of nmdc-ircfrontend:
  -autojoin
        Automatically join clients to the channel (default true)
  -bind string
        The address:port to bind to and listen for clients on (default ":6667")
  -hubsecurity string
        Nick used for administrative events (default "Hub-Security")
  -servername string
        Server name displayed to clients (default "nmdc-ircfrontend")
  -upstream string
        Upstream NMDC/ADC server (default "127.0.0.1:411")
  -verbose
        Display debugging information
```

## Compatibility

*This section was last updated on or around the release of 1.3.0. Current compatibility may differ.*

NMDC's smaller community has standardised around comparatively few protocol implementations by means of necessity. In comparison, there are a lot of IRC client implementations with slightly differing interpretations of the protocol.

For passworded DC usernames, your IRC PASS username must be the same as your IRC nickname.

Tested working:
- AndChat
- AndroIRC
- Atomic
- Hexchat
- HoloIRC
- Irssi
- LiteIRC
- Mango IRC
- mIRC 7
- Mutter
- Revolution IRC
- Weechat
- Yaaic

Unusable:
- Rocket.chat 0.6x IRC plugin (can join room and recieve PMs after some regex tweaking)

## Changelog

2018-03-24 1.3.0
- Feature: Support ADC hubs
- Compatibility: Fix missing userlist in LiteIRC (since 1.2.2)
- Compatibility: Avoid sending empty room list (fixes Yaaic, atomic, Revolution IRC)
- Update libnmdc to 0.17
- [⬇️ nmdc-ircfrontend-1.3.0-win64.7z](https://git.ivysaur.me/attachments/5da75934-863b-4476-86e3-c7da219ee93b) *(818.71 KiB)*
- [⬇️ nmdc-ircfrontend-1.3.0-win32.7z](https://git.ivysaur.me/attachments/7d5b2f9e-6f82-42be-b3ac-5473817cd662) *(766.17 KiB)*
- [⬇️ nmdc-ircfrontend-1.3.0-src.zip](https://git.ivysaur.me/attachments/25b14ccb-82ae-402c-8110-6026b086f4aa) *(23.00 KiB)*
- [⬇️ nmdc-ircfrontend-1.3.0-linux64.tar.xz](https://git.ivysaur.me/attachments/8f4f7592-7070-459f-a2ff-4db910433b60) *(937.04 KiB)*
- [⬇️ nmdc-ircfrontend-1.3.0-linux32.tar.xz](https://git.ivysaur.me/attachments/3ea281d7-2e04-484c-8f0f-70ecfb2918ae) *(876.77 KiB)*

2017-05-28 1.2.3
- Fix a regression with userlist display on HexChat (other IRC client compatibility unknown)
- [⬇️ nmdc-ircfrontend-1.2.3-win64.7z](https://git.ivysaur.me/attachments/19b20903-f173-498a-81bc-e87ab8c22a07) *(751.55 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.3-win32.7z](https://git.ivysaur.me/attachments/30f0a2b7-1f7a-4d15-b70e-bd52f3dde93a) *(687.16 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.3-src.zip](https://git.ivysaur.me/attachments/55a73ae2-96db-4c20-9185-4ce38617053c) *(21.03 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.3-linux64.tar.xz](https://git.ivysaur.me/attachments/ee6992f1-897f-4c8d-bb29-4b9c2a4330df) *(865.07 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.3-linux32.tar.xz](https://git.ivysaur.me/attachments/82f9260c-cfe6-49ea-9a5a-8d6463a399d9) *(799.88 KiB)*

2017-05-27 1.2.2
- Update libnmdc to 0.14
- Fix a crash that could occur if the server is scanned by a non-irc client
- [⬇️ nmdc-ircfrontend-1.2.2-win64.7z](https://git.ivysaur.me/attachments/92ff9cdf-01a7-41bf-82c1-150295509df0) *(751.37 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.2-win32.7z](https://git.ivysaur.me/attachments/450cae51-4d30-46fc-8631-682bbf570128) *(686.93 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.2-src.zip](https://git.ivysaur.me/attachments/a70426b2-d34f-4eef-87a9-3541f28eb04d) *(19.78 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.2-linux64.tar.xz](https://git.ivysaur.me/attachments/d2a2c832-7c1f-4319-bd10-0b7718b56146) *(865.31 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.2-linux32.tar.xz](https://git.ivysaur.me/attachments/1ce22001-dc7f-495b-b384-0e3089392e18) *(799.69 KiB)*

2016-11-29 1.2.1
- Update libnmdc to 0.11
- Fix an issue with -devel version tag in 1.2.0 release binaries
- [⬇️ nmdc-ircfrontend-1.2.1-win64.7z](https://git.ivysaur.me/attachments/a2c89ef5-36c1-41fb-a724-b6665c7ac226) *(750.20 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.1-win32.7z](https://git.ivysaur.me/attachments/3648db7b-12cc-4496-9f0b-ec3dada1a8ff) *(685.56 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.1-src.zip](https://git.ivysaur.me/attachments/97b49b6c-3c36-4eb2-8ca6-b4c9b78ed876) *(20.88 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.1-linux64.tar.xz](https://git.ivysaur.me/attachments/a52a32b1-aca2-4471-99cc-71517e32461f) *(855.09 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.1-linux32.tar.xz](https://git.ivysaur.me/attachments/9b46ee3f-b9d3-4a80-b434-f07952669728) *(798.09 KiB)*

2016-08-27 1.2.0
- Feature: Support WHOIS (display NMDC user's description + client software)
- Feature: `-version` command-line option
- Compatibility: Demote 'Lite IRC' to 'Usable with bugs' section
- Update libnmdc to r9 (fix protocol issues)
- Update golang to 1.7 (smaller binary size)
- [⬇️ nmdc-ircfrontend-1.2.0-win64.7z](https://git.ivysaur.me/attachments/9c35784f-be38-4796-bef1-7fb9a370c2a1) *(747.07 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.0-win32.7z](https://git.ivysaur.me/attachments/f49ff51b-1981-4e26-ad87-80def4a5cd57) *(682.37 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.0-src.zip](https://git.ivysaur.me/attachments/921acc9b-1df9-4445-8192-04effa94cb87) *(20.84 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.0-linux64.tar.xz](https://git.ivysaur.me/attachments/38bdc89f-3519-4217-8950-c13366c1e9c2) *(851.35 KiB)*
- [⬇️ nmdc-ircfrontend-1.2.0-linux32.tar.xz](https://git.ivysaur.me/attachments/1258ef99-e598-4a26-b287-83d0e2cd76cc) *(794.09 KiB)*

2016-05-10 1.1.0
- Feature: Support renaming own client during connection (`/nick`)
- Enhancement: Option to set Hub-Security nick (needed for initial CTCP, upgraded after upstream connection)
- Compatibility: Apply user-agent-specific hacks for mIRC and Atomic clients not nicely handling PRIVMSG with blank sender 
- Compatibility: Better heuristic detection for client tag extraction
- Fix an issue causing the default client tag (nmdc-ircfrontend) to briefly appear in the NMDC user list, by (briefly) delaying the upstream join unless CTCP VERSION response comes in
- Fix an issue generating malformed version numbers in NMDC client tag
- [⬇️ nmdc-ircfrontend-1.1.0-win64.7z](https://git.ivysaur.me/attachments/ab9a32da-cff7-4de9-91d5-fcb7244646c2) *(896.08 KiB)*
- [⬇️ nmdc-ircfrontend-1.1.0-win32.7z](https://git.ivysaur.me/attachments/032a195f-585d-4d85-a904-1f5c4556ac75) *(819.77 KiB)*
- [⬇️ nmdc-ircfrontend-1.1.0-src.zip](https://git.ivysaur.me/attachments/61ae2b92-6502-419b-9d73-79733a820a75) *(20.46 KiB)*
- [⬇️ nmdc-ircfrontend-1.1.0-linux64.tar.xz](https://git.ivysaur.me/attachments/dd4a073d-fe01-4c5d-9eac-27d7c580931c) *(989.59 KiB)*
- [⬇️ nmdc-ircfrontend-1.1.0-linux32.tar.xz](https://git.ivysaur.me/attachments/de5e5bdd-2407-4765-917c-6d7c5df40d82) *(924.09 KiB)*

2016-05-08 1.0.0
- Initial public release
- [⬇️ nmdc-ircfrontend-1.0.0-win64.7z](https://git.ivysaur.me/attachments/a6f42ccb-6b62-408b-af25-7dcfa73d669c) *(890.16 KiB)*
- [⬇️ nmdc-ircfrontend-1.0.0-win32.7z](https://git.ivysaur.me/attachments/b1735675-bbad-473f-9141-c890cac71471) *(817.71 KiB)*
- [⬇️ nmdc-ircfrontend-1.0.0-src.zip](https://git.ivysaur.me/attachments/64c2a8a7-5b7e-46f5-8f30-959f665b616f) *(20.98 KiB)*
- [⬇️ nmdc-ircfrontend-1.0.0-linux64.tar.xz](https://git.ivysaur.me/attachments/0f862b24-f0bb-41a6-bdea-2d27b95a1d00) *(987.32 KiB)*
- [⬇️ nmdc-ircfrontend-1.0.0-linux32.tar.xz](https://git.ivysaur.me/attachments/84471f39-bc5a-47ed-a2ab-0eab66c24ee3) *(922.65 KiB)*
