package main

/*
Copyright (C) 2016-2018 The `nmdc-ircfrontend' author(s)
Copyright (C) 2013 Harry Jeffery

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var (
	APP_VERSION = "0.0" // overridden with build ldflags
)

const (
	APP_NAME             = "nmdc-ircfrontend"
	BLESSED_CHANNEL      = "#chat" // must be lowercase
	BLESSED_CHANNEL_MODE = "n"     // means that you have to be in the channel to chat, but that's it

	NICKS_PER_PROTOMSG     = 128 // Max number of nicks to send per message
	CLIENT_READ_BUFFSIZE   = 512
	CLIENT_KEEPALIVE_EVERY = 60 // should be longer than the client's one, hexchat is 30s
	WAIT_FOR_VERSION       = 3  // Seconds to wait for the CTCP VERSION response before connecting with default MyINFO
)

type replyCode int

const (
	rplWelcome replyCode = iota
	rplJoin
	rplPart
	rplTopic
	rplNoTopic
	rplNames
	rplEndOfNames
	rplWho
	rplEndOfWho
	rplNickChange
	rplKill
	rplMsg
	rplList
	rplListEnd
	rplOper
	rplChannelModeIs
	rplKick
	rplInfo
	rplVersion
	rplMOTDStart
	rplMOTD
	rplEndOfMOTD
	rplPong
	rplWhoisUser
	rplWhoisOperator
	rplEndOfWhois
	errMoreArgs
	errNoNick
	errInvalidNick
	errNickInUse
	errAlreadyReg
	errNoSuchNick
	errUnknownCommand
	errNotReg
	errPassword
	errNoPriv
	errCannotSend
)
