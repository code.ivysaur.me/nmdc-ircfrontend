package libnmdc

import (
	"errors"
	"strconv"
	"time"
)

const (
	DEFAULT_CLIENT_TAG               string        = "libnmdc.go"
	DEFAULT_CLIENT_VERSION           string        = "0.17"
	DEFAULT_HUB_NAME                 string        = "(unknown)"
	SEND_KEEPALIVE_EVERY             time.Duration = 29 * time.Second
	AUTO_RECONNECT_AFTER             time.Duration = 30 * time.Second
	RECONNECT_IF_NO_DATA_RECIEVED_IN time.Duration = 24 * time.Hour // we expect keepalives wayyyy more frequently than this
	AUTODETECT_ADC_NMDC_TIMEOUT      time.Duration = 3 * time.Second
)

var ErrNotConnected error = errors.New("Not connected")

func maybeParse(str string, dest *uint64, default_val uint64) {
	sz, err := strconv.ParseUint(str, 10, 64)
	if err == nil {
		*dest = sz
	} else {
		*dest = default_val
	}
}
