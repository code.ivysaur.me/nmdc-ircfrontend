package libnmdc

type UserCommandType uint8

const (
	USERCOMMAND_TYPE_SEPARATOR   UserCommandType = 0
	USERCOMMAND_TYPE_RAW         UserCommandType = 1
	USERCOMMAND_TYPE_NICKLIMITED UserCommandType = 2
	USERCOMMAND_TYPE_CLEARALL    UserCommandType = 255
)

type UserCommandContext uint8

const (
	USERCOMMAND_CONTEXT_HUB      UserCommandContext = 1
	USERCOMMAND_CONTEXT_USER     UserCommandContext = 2
	USERCOMMAND_CONTEXT_SEARCH   UserCommandContext = 4
	USERCOMMAND_CONTEXT_FILELIST UserCommandContext = 8
)

type UserCommand struct {
	Type       UserCommandType
	Context    UserCommandContext
	Message    string
	Command    string
	RemoveThis bool // Currently only set by ADC hubs
}
