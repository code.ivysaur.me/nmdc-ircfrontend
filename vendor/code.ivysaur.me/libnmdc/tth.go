package libnmdc

import (
	"encoding/base32"
	"errors"
	"strings"

	"github.com/cxmcc/tiger"
)

// Base32 encodes the input slice in BASE32 string format without any trailing
// padding equals characters.
func Base32(input []byte) string {
	return strings.TrimRight(base32.StdEncoding.EncodeToString(input), "=")
}

// TTH returns the TTH hash of a string in raw byte format. Use the Base32()
// function to convert it to normal string format.
// This is a basic implementation that only supports content up to 1024 bytes in length.
func TTH(input string) ([]byte, error) {

	// Short segments do not need to be padded.
	// Content above 1024 bytes needs tree handling (0x00 prefix for leaf nodes,
	// 0x01 prefix for hash nodes) but for content less than 1024 bytes, just
	// return the leaf hash
	// @ref http://adc.sourceforge.net/draft-jchapweske-thex-02.html
	if len(input) > 1024 {
		return nil, errors.New("TTH content exceeded 1024 bytes")
	}

	// Single leaf hash only
	leafHash := tiger.New()
	leafHash.Write([]byte("\x00" + input))
	return leafHash.Sum(nil), nil
}

func Tiger(input string) []byte {
	leafHash := tiger.New()
	leafHash.Write([]byte(input))
	return leafHash.Sum(nil)
}
